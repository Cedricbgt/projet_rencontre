from django.contrib import admin

# Register your models here.
from app.models import Tag, Person, Category

admin.site.register(Tag)
admin.site.register(Person)
admin.site.register(Category)

