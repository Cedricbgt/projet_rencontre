from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Category(models.Model):
    description = models.CharField(max_length=200, blank=True, null=True, default=None)

    def __str__(self):
        return f'{str(self.description)}'


class Tag(models.Model):
    description = models.CharField(max_length=200, blank=True, null=True)
    categories = models.ManyToManyField(Category)

    def __str__(self):
        return '{} ({})'.format(self.description, '/'.join([str(c) for c in self.categories.all()]))


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    naissance = models.CharField(max_length=150, blank=False, null=False)

    description = models.TextField(blank=True, null=True, default=None)

    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return '{} ({})'.format(self.user, '/'.join([str(c) for c in self.tags.all()]))
